// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

for (var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}
// jawaban soal 1
// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// soal 2
function introduce(data) {
    return 'Nama saya ' + data.name + ', umur saya ' + data.age + ', alamat saya di ' + data.address + ', dan saya punya hobby yaitu ' + data.hobby;
}
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan);
// jawaban soal 3
// Nama saya John, umur saya 30, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming

// soal

function hitung_huruf_vokal(hitung) {
    var lower = hitung.toLowerCase();
    var array = lower.split("");
    var vokal = 0;
    for (var j = 0; j < array.length; j++) {
        if (array[j] == 'a') {
            vokal++;
        } else if (array[j] == 'i') {
            vokal++;
        } else if (array[j] == 'u') {
            vokal++;
        } else if (array[j] == 'e') {
            vokal++;
        } else if (array[j] == 'o') {
            vokal++;
        }
    }

    return hitung = vokal;
}

var hitung_1 = hitung_huruf_vokal("Septianyudis");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1);
console.log(hitung_2);

// jawaban soal 3
// 3 2

// soal 4
function hitung(angka) {
    return 2 * angka + -2;
}
console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(4)) // 6
console.log(hitung(5)) // 8
    //     //jawaban no 4