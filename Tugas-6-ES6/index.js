// soal 1
const luas = () => {
    let p = 4;
    let l = 7;
    let hasil = 2 * (p + l);
    return console.log(hasil)
}

luas();
// jawaban soal 1 = 22

// soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`);
        }
    }
}

newFunction("William", "Imoh").fullName()
    // Jawaban Soal 2 Willian Imoh

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);
// jawaban soal 3 Muhammad Iqbal Mubarok Jalan Ranamanyar playing football

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

let combineArray = [...west, ...east];
console.log(combined);
// jawaban soal 4 
// ['Will','Chris','Sam','Holly','Gill', 'Brian','Noel', 'Maggie']

// soal 5
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before);
// jawaban soal 5 Lorem glass dolor sit amet, consectetur adipiscing elit, earth