// soal 1
var readBooks = require('./callback.js')
var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

function mulai(time, books, i) {
    if (i < 4) {
        readBooks(time, books[0], function(sisa) {
            if (sisa > 0) {
                mulai(sisa, books, i[0]);
            }
            readBooks(sisa, books[1], function(sisa1) {
                if (sisa1 > 0) {
                    mulai(sisa1, books, i[1]);
                }
                readBooks(sisa1, books[2], function(sisa2) {
                    if (sisa2 > 0) {
                        mulai(sisa2, books, i[2]);
                    }
                    readBooks(sisa2, books[3], function(sisa3) {
                        if (sisa3 > 0) {
                            mulai(sisa3, books, i[2]);
                        }
                    })
                })
            })
        })
    }
}
mulai(10000, books, 0);
// jawaban soal 1 
// saya membaca LOTR
// saya sudah membaca LOTR, sisa waktu saya 7000
// saya membaca Fidas
// saya sudah membaca Fidas, sisa waktu saya 5000
// saya membaca Kalkulus
// saya sudah membaca Kalkulus, sisa waktu saya 1000
// saya membaca komik
// saya sudah membaca komik, sisa waktu saya 0


// soal 2
var readBooksPromise = require('./promise.js')

function baca(time, i, sisa) {
    readBooksPromise(time, books[i])
        .then(function(remainingTime) {
            time = remainingTime;
            sisa = sisa - 1;
            if (sisa > 0) {
                baca(time, i + 1, sisa);
            }
        })
        .catch(function(error) {})
}
baca(10000, 0, 4);
// soal 2
// saya mulai membaca LOTR
// saya sudah selesai membaca LOTR, sisa waktu saya 7000
// saya mulai membaca Fidas
// saya sudah selesai membaca Fidas, sisa waktu saya 5000
// saya mulai membaca Kalkulus
// saya sudah selesai membaca Kalkulus, sisa waktu saya 1000
// saya mulai membaca komik
// saya sudah selesai membaca komik, sisa waktu saya 0