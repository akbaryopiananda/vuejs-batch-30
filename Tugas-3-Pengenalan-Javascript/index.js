// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// saya senang belajar JAVASCRIPT jawaban soal 1
console.log(pertama.substr(0, 4).concat(pertama.substring(11, 19)).concat(kedua.substr(0, 8)).concat(kedua.substring(8, 19).toUpperCase()));

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var numPertama = parseInt(kataPertama);
var numKedua = parseInt(kataKedua);
var numKetiga = parseInt(kataKetiga);
var numKeempat = parseInt(kataKeempat);
// 24 jawaban soal 2
var hasil = numPertama + numKedua * numKetiga + numKeempat;
console.log(hasil);

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali 
// Jawaban soal 3
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
