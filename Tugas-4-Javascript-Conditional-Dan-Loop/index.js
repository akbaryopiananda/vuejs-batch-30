// Soal 1
var nilai = 74;

if (nilai >= 85) {
    console.log('indeksnya A');
} else if (nilai >= 75) {
    console.log('indeksnya B');
} else if (nilai >= 65) {
    console.log('indeksnya C');
} else if (nilai >= 55) {
    console.log('indeksnya D');
} else {
    console.log('indeksnya E');
}

// "indeksnya C" jawaban soal 1

// soal 2
var tanggal = 12;
var bulan = 4;
var tahun = 2002;

switch (bulan) {
    case 1:
        { console.log(tanggal + ' Januari ' + tahun); break }
    case 2:
        { console.log(tanggal + ' Februari ' + tahun); break }
    case 3:
        { console.log(tanggal + ' Maret ' + tahun); break }
    case 4:
        { console.log(tanggal + ' April ' + tahun); break }
    case 5:
        { console.log(tanggal + ' Mei ' + tahun); break }
    case 6:
        { console.log(tanggal + ' Juni ' + tahun); break }
    case 7:
        { console.log(tanggal + ' Juli ' + tahun); break }
    case 8:
        { console.log(tanggal + ' Agustus ' + tahun); break }
    case 9:
        { console.log(tanggal + ' September ' + tahun); break }
    case 10:
        { console.log(tanggal + ' Oktober ' + tahun); break }
    case 11:
        { console.log(tanggal + ' November ' + tahun); break }
    case 12:
        { console.log(tanggal + ' Desember ' + tahun); break }
}

// "12 April 2002" Jawaban soal 2

// soal 3
var n = 7;
// ganti n untuk test -> 3 / 7
var out = '';
for (var tn = 0; tn < n; tn++) {
    for (var nn = 0; nn <= tn; nn++) {
        out += '#';
    }
    out += '\n';
}
console.log(out);
// #
// ##
// ### (n=3)

// #
// ##
// ###
// ####
// #####
// ######
// ####### (n=7)
// jawaban soal 3

// soal 4
let index = 0;
let no = 1;
m = 10;
while (no <= m) {
    if (index == 0) {
        console.log(no + " - I love Programming");
    } else if (index == 1) {
        console.log(no + " - I love Javascript");
    } else if (index == 2) {
        console.log(no + " - I love VueJS");
    }
    index += 1;
    no += 1;

    if (index == 3) {
        console.log("=".repeat(no - 1));
        index = 0;
    }
}
// jawaban soal 4
// 1 - I love Programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love Programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love Programming
// 8 - I love Javascript
// 9 - I love VueJS
// =========
// 10 - I love Programming