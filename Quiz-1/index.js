// Soal 1
var tanggal = 29;
var bulan = 6;
var tahun = 2021;

function next_date(tanggal, bulan, tahun) {
    switch (bulan) {
        case 1:
            { return tanggal + ' Januari ' + tahun; break }
        case 2:
            { return tanggal + ' Februari ' + tahun; break }
        case 3:
            { return tanggal + ' Maret ' + tahun; break }
        case 4:
            { return tanggal + ' April ' + tahun; break }
        case 5:
            { return tanggal + ' Mei ' + tahun; break }
        case 6:
            { return tanggal + ' Juni ' + tahun; break }
        case 7:
            { return tanggal + ' Juli ' + tahun; break }
        case 8:
            { return tanggal + ' Agustus ' + tahun; break }
        case 9:
            { return tanggal + ' September ' + tahun; break }
        case 10:
            { return tanggal + ' Oktober ' + tahun; break }
        case 11:
            { return tanggal + ' November ' + tahun; break }
        case 12:
            { return tanggal + ' Desember ' + tahun; break }
    }
}

console.log(next_date(tanggal, bulan, tahun));
// 29 Juni 2021

// soal 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(kalimat) {
    var kata = kalimat.trim(' ');
    return kata.split(' ').length;
}

console.log(jumlah_kata(kalimat_1)); // 6
console.log(jumlah_kata(kalimat_2)); // 2
console.log(jumlah_kata(kalimat_3)); // 4
//jawaban soal 2